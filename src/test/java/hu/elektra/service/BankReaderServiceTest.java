package hu.elektra.service;

import hu.elektra.core.Bank;
import hu.elektra.util.FileUtil;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Created by attila on 24/05/17.
 */
public class BankReaderServiceTest {

    private static InputStream excelFile;

    @BeforeClass
    public static void setup() throws FileNotFoundException {
        excelFile= FileUtil.findResourceOnClasspath("test_bank_data.xls");
    }


    @Test
    public void testExcelReading() throws IOException {
        BankReaderService service = new BankReaderService();
        List<Bank> bank = service.readExcel(excelFile);
        System.out.println(bank.size());
        System.out.println(bank.get(0).getOsszeg());
        Assert.assertTrue(36525 == bank.get(0).getOsszeg());
    }

    @Test
    public void testCompletaableFuture() {
        long started = System.currentTimeMillis();

        // configure CompletableFuture
        CompletableFuture<Integer> futureCount = createCompletableFuture();

        // continue to do other work
        System.out.println("Took " + (started - System.currentTimeMillis()) + " milliseconds" );

        // now its time to get the result
        try {
            int count = futureCount.get();
            System.out.println("CompletableFuture took " + (started - System.currentTimeMillis()) + " milliseconds" );

            System.out.println("Result " + count);
        } catch (InterruptedException | ExecutionException ex) {
            // Exceptions from the future should be handled here
        }
    }

    private CompletableFuture<Integer> createCompletableFuture() {
        CompletableFuture<Integer> futureCount = CompletableFuture.supplyAsync(
                () -> {
                    try {
                        // simulate long running task
                        Thread.sleep(5000);
                    } catch (InterruptedException e) { }
                    return 20;
                });
        return futureCount;
    }

    @Test
    public void testFolderList() throws URISyntaxException {
        String folder = "/media/picur/Data/hackintosh_d/pics/pen/006/";
        Path dir = Paths.get(folder);
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, (entry)-> {
                return entry.getFileName().toString().startsWith("2017_05_06_22_45_05");
            })
        ) {
            for (Path path: stream) {
                System.out.println(path.getFileName());
                File newFile = new File("valami.txt");
                System.out.println(newFile.getAbsolutePath());
//                File file = path.toFile();
//                File newFile = new File(folder + file.getName().substring(1));
//                file.renameTo(newFile);
            }
        } catch (IOException | DirectoryIteratorException x) {
            // IOException can never be thrown by the iteration.
            // In this snippet, it can only be thrown by newDirectoryStream.
            System.err.println(x);
        }
    }

}
