package hu.elektra.service;

import hu.elektra.core.Bank;
import hu.elektra.core.Szamla;
import hu.elektra.core.Tetel;
import hu.elektra.core.UgyfelSzamlak;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by attila on 12/06/17.
 */
public class BefizetesAssignerServiceTest {

    private static List<Bank> bankData;
    private static List<UgyfelSzamlak> ugyfelek;
    private static List<Szamla> szamlak;

    @Before
    public void setup(){
        bankData = new ArrayList<>();
        bankData.add(Bank.builder().datum(new Date())
                .osszeg(3000.0)
                .leiras("Bejövő giro jóváírás   SPIITR0050786559\n50400096-11089225-00000000\nPEDRO-TREND Kft.\nKözlemény: 11441sz\nÉrtéknap: 2017.03.01")
                .build());
        bankData.add(Bank.builder().datum(new Date())
                .osszeg(0.0)
                .leiras("JÓVÁIRÁS   YU2920423\n10700017-53811000-49900004G/000000\nBA-000000000000070311001302\nFL.DAT-170309 BEFDAT-170308     0g3\nÉrtéknap: 2017.03.10")
                .build());
        bankData.add(Bank.builder().datum(new Date())
                .osszeg(0.0)
                .leiras("JÓVÁIRÁS   YU2842626\n10700017-53811000-49900004G/000000\nBA-000000000000070311002631\nFL.DAT-170302 BEFDAT-170301     0aJ\nÉrtéknap: 2017.03.03")
                .build());
        bankData.add(Bank.builder().datum(new Date())
                .osszeg(0.0)
                .leiras("Bejövő giro jóváírás   SPIITR0051153049\n11773315-01130306-00000000\nKOSZTICS GERGŐ\nKözlemény: EB telefonszámla\nÉrtéknap: 2017.03.10")
                .build());
        bankData.add(Bank.builder().datum(new Date())
                .osszeg(0.0)
                .leiras("Bejövő giro jóváírás   SPIITR0051173168\n10918001-00000058-65370002\nSTRUCTURAIL MÉRNÖKI TERVEZŐ ÉS TANÁ\nKözlemény: P0011597\nÉrtéknap: 2017.03.10")
                .build());
        bankData.add(Bank.builder().datum(new Date())
                .osszeg(0.0)
                .leiras("Bejövő giro jóváírás   SPIITR0051204488\n11600006-00000000-66265038\nPURTECH KFT\nKözlemény: 11584 szla\nÉrtéknap: 2017.03.10")
                .build());
        ugyfelek = new ArrayList<>();
        ugyfelek.add(UgyfelSzamlak.builder().nev("PEDRO-TREND Kft.").ugyfelKod("31100200").ugyfelId(1L).szamlaSorszamok("").build());
        ugyfelek.add(UgyfelSzamlak.builder().nev("Valami kft.").ugyfelKod("31100130").ugyfelId(2L).szamlaSorszamok("").build());
        ugyfelek.add(UgyfelSzamlak.builder().nev("Kosztics Gergő").ugyfelKod("31100236").ugyfelId(3L).szamlaSorszamok("").build());
        ugyfelek.add(UgyfelSzamlak.builder().nev("Structurail Kft.").ugyfelKod("31100149").ugyfelId(4L).szamlaSorszamok("P0011597,P0011729,P0011856,P0011983").build());
        ugyfelek.add(UgyfelSzamlak.builder().nev("Purtech Kft.").ugyfelKod("31100074").ugyfelId(5L).szamlaSorszamok("").build());

        szamlak = new ArrayList<>();
        szamlak.add(buildSzamla("P0011597", 1000, 1000, 1000));
        szamlak.add(buildSzamla("P0011598", 1000, 500, 500, 200));

    }

    private Szamla buildSzamla(String szamlaSorszam, double... tetelBrutto){
        List<Tetel> lstTetelek = new ArrayList<>();
        for (double tetel : tetelBrutto){
            lstTetelek.add(Tetel.builder().bruttoar(tetel).build());
        }
        Tetel[] tetelek = lstTetelek.toArray(new Tetel[lstTetelek.size()]);
        return Szamla.builder().szamlaTetelek(tetelek).szamlaSorszam(szamlaSorszam).build();
    }

    @Test
    public void testAssigner(){
        BefizetesAssignerService service = new BefizetesAssignerService();
        service.assignBefizetesToUgyfel(bankData, ugyfelek);
        System.out.println(bankData.get(4));
        Assert.assertEquals("31100200", bankData.get(0).getUgyfelKod());
        Assert.assertEquals("31100130", bankData.get(1).getUgyfelKod());
        Assert.assertEquals("31100236", bankData.get(3).getUgyfelKod());
        Assert.assertEquals("31100074", bankData.get(5).getUgyfelKod());
        Assert.assertEquals("31100149", bankData.get(4).getUgyfelKod());
    }

    @Test
    public void testAssignBasedOnValue(){
        BefizetesAssignerService service = new BefizetesAssignerService();
        service.assignSzamlaBasedOnValue(bankData.get(0), szamlak);
        Assert.assertEquals("P0011597", bankData.get(0).getSzamlaSorszamok()[0]);
    }
}