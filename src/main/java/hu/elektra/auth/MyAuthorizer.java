package hu.elektra.auth;

import io.dropwizard.auth.Authorizer;

import java.security.Principal;

public class MyAuthorizer implements Authorizer<Principal> {
    @Override
    public boolean authorize(Principal user, String role) {
        return user.getName().equals("admin") && role.equals("ADMIN");
    }
}