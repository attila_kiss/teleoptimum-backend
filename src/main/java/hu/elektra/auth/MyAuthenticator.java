package hu.elektra.auth;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;
import java.security.Principal;
import java.util.Optional;

public class MyAuthenticator implements Authenticator<BasicCredentials, Principal> {
    @Override
    public Optional<Principal> authenticate(BasicCredentials credentials) throws AuthenticationException {
        if ("TO1471".equals(credentials.getPassword())) {
            return Optional.of(() -> credentials.getUsername());
        }
        return Optional.empty();
    }
}