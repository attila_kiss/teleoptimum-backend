package hu.elektra;

import hu.elektra.util.Elektra;
import hu.elektra.util.MailSettings;
import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.*;

public class TeleoptimumConfiguration extends Configuration {

    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    private Elektra elektra = new Elektra();

    private MailSettings mailSettings = new MailSettings();

    @JsonProperty("database")
    public void setDataSourceFactory(DataSourceFactory factory) {
        this.database = factory;
    }

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    @JsonProperty("elektra")
    public void setElektra(Elektra elektra){
        this.elektra = elektra;
    }

    @JsonProperty("elektra")
    public Elektra getElektra(){
        return this.elektra;
    }

    @JsonProperty("mail")
    public void setMailSettings(MailSettings mailSettings){
        this.mailSettings = mailSettings;
    }

    @JsonProperty("mail")
    public MailSettings getMailSettings(){
        return this.mailSettings;
    }
}
