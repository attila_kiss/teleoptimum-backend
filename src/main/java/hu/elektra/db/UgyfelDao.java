package hu.elektra.db;

import hu.elektra.core.Telefonszam;
import hu.elektra.core.Ugyfel;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(Ugyfel.UgyfelMapper.class)
public interface UgyfelDao {

    @SqlQuery("select * from ugyfel order by nev")
    List<Ugyfel> list();

    @SqlUpdate("UPDATE ugyfel SET ugyfel_tipus = :ugyfelTipus, email = :email, fizetesi_mod = :fizetesiMod, needs_special_invoice = :needsSpecialInvoice, nev = :nev, " +
            "szamla_szam = :szamlaSzam, ugyfel_kod = :ugyfelKod, adoszam = :adoszam, ceg_jegyzek_szam = :ceg_jegyzek_szam, kacsolat_tarto_nev = :kacsolat_tarto_nev, " +
            "kapcsolat_tarto_sz_ig_szam = :kapcsolat_tarto_sz_ig_szam, kapcsolat_tarto_tsz = :kapcsolat_tarto_tsz, anyja_neve = :anyja_neve, egyeb_telefonszam = :egyeb_telefonszam, " +
            "sz_hely_ido = :sz_hely_ido, szem_ig_szam = :szem_ig_szam, cimek = :cimek WHERE ugyfel_id = :id;")
    int updateUgyfel(@Bind("id") long id,
                     @Bind("ugyfelTipus") String ugyfelTipus,
                     @Bind("email") String email,
                     @Bind("fizetesiMod") String fizetesiMod,
                     @Bind("needsSpecialInvoice") Short needsSpecialInvoice,
                     @Bind("nev") String nev,
                     @Bind("szamlaSzam") String szamlaSzam,
                     @Bind("ugyfelKod") String ugyfelKod,
                     @Bind("adoszam") String adoszam,
                     @Bind("ceg_jegyzek_szam") String ceg_jegyzek_szam,
                     @Bind("kacsolat_tarto_nev") String kacsolat_tarto_nev,
                     @Bind("kapcsolat_tarto_sz_ig_szam") String kapcsolat_tarto_sz_ig_szam,
                     @Bind("kapcsolat_tarto_tsz") String kapcsolat_tarto_tsz,
                     @Bind("anyja_neve") String anyja_neve,
                     @Bind("egyeb_telefonszam") String egyeb_telefonszam,
                     @Bind("sz_hely_ido") String sz_hely_ido,
                     @Bind("szem_ig_szam") String szem_ig_szam,
                     @Bind("cimek") String cimek);

    @SqlUpdate("INSERT INTO ugyfel\n" +
            "(ugyfel_id,\n" +
            "ugyfel_tipus,\n" +
            "email,\n" +
            "fizetesi_mod,\n" +
            "needs_special_invoice,\n" +
            "nev,\n" +
            "szamla_szam,\n" +
            "ugyfel_kod,\n" +
            "adoszam,\n" +
            "ceg_jegyzek_szam,\n" +
            "kacsolat_tarto_nev,\n" +
            "kapcsolat_tarto_sz_ig_szam,\n" +
            "kapcsolat_tarto_tsz,\n" +
            "anyja_neve,\n" +
            "egyeb_telefonszam,\n" +
            "sz_hely_ido,\n" +
            "szem_ig_szam,\n" +
            "cimek)\n" +
            "VALUES\n" +
            "(:id,\n" +
            ":ugyfelTipus,\n" +
            ":email,\n" +
            ":fizetesiMod,\n" +
            ":needsSpecialInvoice,\n" +
            ":nev,\n" +
            ":szamlaSzam,\n" +
            ":ugyfelKod,\n" +
            ":adoszam,\n" +
            ":ceg_jegyzek_szam,\n" +
            ":kacsolat_tarto_nev,\n" +
            ":kapcsolat_tarto_sz_ig_szam,\n" +
            ":kapcsolat_tarto_tsz,\n" +
            ":anyja_neve,\n" +
            ":egyeb_telefonszam,\n" +
            ":sz_hely_ido,\n" +
            ":szem_ig_szam,\n" +
            ":cimek);")
    int insertUgyfel(@Bind("id") long id,
                     @Bind("ugyfelTipus") String ugyfelTipus,
                     @Bind("email") String email,
                     @Bind("fizetesiMod") String fizetesiMod,
                     @Bind("needsSpecialInvoice") Short needsSpecialInvoice,
                     @Bind("nev") String nev,
                     @Bind("szamlaSzam") String szamlaSzam,
                     @Bind("ugyfelKod") String ugyfelKod,
                     @Bind("adoszam") String adoszam,
                     @Bind("ceg_jegyzek_szam") String ceg_jegyzek_szam,
                     @Bind("kacsolat_tarto_nev") String kacsolat_tarto_nev,
                     @Bind("kapcsolat_tarto_sz_ig_szam") String kapcsolat_tarto_sz_ig_szam,
                     @Bind("kapcsolat_tarto_tsz") String kapcsolat_tarto_tsz,
                     @Bind("anyja_neve") String anyja_neve,
                     @Bind("egyeb_telefonszam") String egyeb_telefonszam,
                     @Bind("sz_hely_ido") String sz_hely_ido,
                     @Bind("szem_ig_szam") String szem_ig_szam,
                     @Bind("cimek") String cimek);

    @SqlQuery("SELECT max(ugyfel_kod) FROM ugyfel;")
    int getLatestUgyfelKod();
    
    @SqlQuery("SELECT max(ugyfel_id) FROM ugyfel;")
    long getLatestUgyfelId();
}
