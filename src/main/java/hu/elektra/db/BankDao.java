package hu.elektra.db;

import hu.elektra.core.Bank;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(Bank.BankMapper.class)
public interface BankDao {

    @SqlQuery("select * from bank order by datum")
    List<Bank> list();

    @SqlUpdate("insert into bank (datum, leiras, osszeg, ugyfel_kod, szamla_sorszam) values (:datum, :leiras, :osszeg, :ugyfelKod, :szamlaSorszamok)")
    int insertBank(@Bind("datum")Date datum, @Bind("leiras")String leiras, @Bind("osszeg")Double osszeg, @Bind("ugyfelKod")String ugyfelKod, @Bind("szamlaSorszamok")String szamlaSorszamok);

    @SqlUpdate("UPDATE bank SET datum = :datum, leiras = :leiras, osszeg = :osszeg, ugyfel_kod = :ugyfelKod, szamla_sorszam = :szamlaSorszamok WHERE datum = :datum AND leiras = :leiras")
    void updateBank(@Bind("datum")Date datum, @Bind("leiras")String leiras, @Bind("osszeg")Double osszeg, @Bind("ugyfelKod")String ugyfelKod, @Bind("szamlaSorszamok")String szamlaSorszamok);

}
