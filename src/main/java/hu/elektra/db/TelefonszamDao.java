package hu.elektra.db;

import hu.elektra.core.Telefonszam;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(Telefonszam.TelefonszamMapper.class)
public interface TelefonszamDao {

    @SqlQuery("select * from telefonszam")
    List<Telefonszam> list();

    @SqlUpdate("insert into telefonszam (telefonszam_id, telefonszam, ugyfel_id) values (:id, :telefonszam, :ugyfelId)")
    void insert(@BindBean Telefonszam telefonszam);

    @SqlUpdate("update telefonszam set telefonszam = :telefonszam, ugyfel_id = :ugyfelId where telefonszam_id = :id")
    void update(@BindBean Telefonszam telefonszam);

    @SqlUpdate("delete from telefonszam where telefonszam_id = :id")
    void delete(@Bind("id") long id);
}
