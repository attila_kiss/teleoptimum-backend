package hu.elektra.db;

import hu.elektra.core.Szamla;
import hu.elektra.core.UgyfelSzamlak;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(Szamla.SzamlaMapper.class)
public interface SzamlaDao {

    @SqlBatch("INSERT INTO szamla (szamla_id, nev,  cim, adoszam, fizetesi_mod, kelte, hatarido, teljesites, idoszak_kezdete, idoszak_vege, szamla_tetelek, reszletezo_tetelek, ugyfel_kod, telenor_szamla_sorszam, email) VALUES " +
            "(:id, :nev, :cim, :adoszam, :fizetesiMod, :kelte, :hatarido, :teljesites, :idoszakKezdete, :idoszakVege, :szamlaTetelek, :reszletezoTetelek, :ugyfelKod, :telenorSzamlaSorszam, :email)")
    int[] bulkinsertSzamla(@Bind("id") List<Long> id,
                          @Bind("nev") List<String> nev,
                          @Bind("cim") List<String> cim,
                          @Bind("adoszam") List<String> adoszam,
                          @Bind("fizetesiMod") List<String> fizetesiMod,
                          @Bind("kelte") List<Date> szamlaKelte,
                          @Bind("hatarido") List<Date> fizetesiHatarido,
                          @Bind("teljesites") List<Date> teljesites,
                          @Bind("idoszakKezdete") List<Date> idoszakKezdete,
                          @Bind("idoszakVege") List<Date> idoszakVege,
                          @Bind("szamlaTetelek") List<String> szamlaTetelek,
                          @Bind("reszletezoTetelek") List<String> reszletezoTetelek,
                          @Bind("ugyfelKod") List<String> ugyfelKod,
                          @Bind("telenorSzamlaSorszam") List<String> telenorSzamlaSorszam,
                          @Bind("email") List<String> email);

    @SqlUpdate("INSERT INTO szamla (szamla_id, nev,  cim, adoszam, fizetesi_mod, kelte, hatarido, teljesites, idoszak_kezdete, idoszak_vege, szamla_tetelek, reszletezo_tetelek, ugyfel_kod, email) VALUES (:id, :nev, :cim, :adoszam, :fizetesiMod, :kelte, :hatarido, :teljesites, :idoszakKezdete, :idoszakVege, :szamlaTetelek, :reszletezoTetelek, :ugyfelKod, :email)")
    void insertSzamla(@Bind("id") long id,
                          @Bind("nev") String nev,
                          @Bind("cim") String cim,
                          @Bind("adoszam") String adoszam,
                          @Bind("fizetesiMod") String fizetesiMod,
                          @Bind("kelte") Date szamlaKelte,
                          @Bind("hatarido") Date fizetesiHatarido,
                          @Bind("teljesites") Date teljesites,
                          @Bind("idoszakKezdete") Date idoszakKezdete,
                          @Bind("idoszakVege") Date idoszakVege,
                          @Bind("szamlaTetelek") String szamlaTetelek,
                          @Bind("reszletezoTetelek") String reszletezoTetelek,
                          @Bind("ugyfelKod") String ugyfelKod,
                          @Bind("email") String email
    );

    @SqlQuery("select * from szamla")
    List<Szamla> list();

    @SqlQuery("select * from szamla where telenor_szamla_sorszam = :telenorSzamlaSorszam")
    List<Szamla> listByTelenorSzamlaSorszam(@Bind("telenorSzamlaSorszam") String telenorSzamlaSorszam);

    @SqlUpdate("UPDATE szamla SET szamla_kep_url = :szamlaKepUrl," +
            "csekk_url = :csekkUrl, szamla_sorszam = :szamlaSorszam where szamla_id = :id")
    int updateSzamlakep(@BindBean Szamla szamla);

    @SqlQuery("select telenor_szamla_sorszam from szamla")
    List<String> listTelenorSzamlaSorszamok();

    @SqlQuery("select * from szamla where kelte >= :from and kelte <= :to order by szamla_sorszam ASC")
    List<Szamla> findSzamlaByKelteBetween(@Bind("from")Date from, @Bind("to")Date to);

    @SqlQuery("SELECT * from szamla where szamla_id in :ids")
    List<Szamla> findSzamlakById(@Bind("ids")List<Long> ids);

    @SqlQuery("SELECT * from szamla where szamla_id = :id")
    Szamla findSzamlaById(@Bind("id") Long id);

    @SqlQuery("select u.ugyfel_id, sz.nev, sz.ugyfel_kod, GROUP_CONCAT(sz.szamla_sorszam) as szamlak from szamla as sz join ugyfel as u on sz.ugyfel_kod = u.ugyfel_kod where u.ugyfel_id != 1 group by sz.nev")
    @Mapper(UgyfelSzamlak.UgyfelSzamlakMapper.class)
    List<UgyfelSzamlak> listSzamlak();

    @SqlQuery("select * from szamla where ugyfel_kod = :ugyfelKod")
    List<Szamla> findSzamlakByUgyfelKod(@Bind("ugyfelKod") String ugyfelKod);

    @SqlQuery("select max(szamla_id) from szamla")
    Long getLatestIndex();
}
