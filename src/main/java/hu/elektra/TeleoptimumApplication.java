package hu.elektra;

import hu.elektra.auth.MyAuthenticator;
import hu.elektra.auth.MyAuthorizer;
import hu.elektra.db.BankDao;
import hu.elektra.db.SzamlaDao;
import hu.elektra.db.TelefonszamDao;
import hu.elektra.db.UgyfelDao;
import hu.elektra.pdf.CsekkNyomtato;
import hu.elektra.pdf.SzamlazoProgramConnector;
import hu.elektra.resources.BankResource;
import hu.elektra.service.BankReaderService;
import hu.elektra.service.BefizetesAssignerService;
import hu.elektra.service.EmailSenderService;
import hu.elektra.resources.SzamlaResource;
import hu.elektra.resources.TelefonszamResource;
import hu.elektra.resources.UgyfelResource;
import hu.elektra.service.PdfRenderService;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.h2.tools.Server;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class TeleoptimumApplication extends Application<TeleoptimumConfiguration> {

    public static Server server;

    public static void main(final String[] args) throws Exception {
        new TeleoptimumApplication().run(args);
        String connection = "-tcpAllowOthers";
        server = Server.createTcpServer(connection).start();
    }

    @Override
    public String getName() {
        return "teleoptimum";
    }

    @Override
    public void initialize(final Bootstrap<TeleoptimumConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets/", "/"));
        bootstrap.addBundle(new MultiPartBundle());
    }

    @Override
    public void run(final TeleoptimumConfiguration configuration,
                    final Environment environment) {

        // Enable CORS headers
        final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        final TelefonszamDao telefonszamDao = jdbi.onDemand(TelefonszamDao.class);
        final UgyfelDao ugyfelDao = jdbi.onDemand(UgyfelDao.class);
        final SzamlaDao szamlaDao = jdbi.onDemand(SzamlaDao.class);
        final BankDao bankDao = jdbi.onDemand(BankDao.class);
        final CsekkNyomtato csekkNyomtato = new CsekkNyomtato();
        final SzamlazoProgramConnector szamlazoProgramConnector = new SzamlazoProgramConnector(csekkNyomtato, configuration.getElektra(), szamlaDao);
        final EmailSenderService emailSenderService = new EmailSenderService(configuration.getElektra(), configuration.getMailSettings());
        final BankReaderService bankReaderService = new BankReaderService();
        final BefizetesAssignerService befizetesAssignerService = new BefizetesAssignerService();
        final PdfRenderService pdfRenderService = new PdfRenderService(configuration.getElektra());

        environment.jersey().register(new TelefonszamResource(telefonszamDao));
        environment.jersey().register(new UgyfelResource(ugyfelDao, telefonszamDao));
        environment.jersey().register(new SzamlaResource(szamlaDao, szamlazoProgramConnector, configuration.getElektra(), emailSenderService, jdbi, pdfRenderService));
        environment.jersey().register(new BankResource(bankDao, bankReaderService, befizetesAssignerService, szamlaDao, jdbi));
        environment.jersey().register(new AuthDynamicFeature(
                new BasicCredentialAuthFilter.Builder<>()
                        .setAuthenticator(new MyAuthenticator())
                        .buildAuthFilter()));

    }

}
