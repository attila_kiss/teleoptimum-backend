package hu.elektra.pdf;

        import com.itextpdf.text.DocumentException;
        import com.itextpdf.text.pdf.AcroFields;
        import com.itextpdf.text.pdf.BaseFont;
        import com.itextpdf.text.pdf.PdfReader;
        import com.itextpdf.text.pdf.PdfStamper;

        import java.io.*;

        import hu.elektra.core.Szamla;
        import hu.elektra.core.Ugyfel;
        import hu.elektra.util.DateUtil;
        import hu.elektra.util.FileUtil;
        import hu.elektra.util.NumberUtil;
        import org.apache.commons.io.IOUtils;

public class CsekkNyomtato {

    public static final String tmpFolder = "/tmp/csekk/";


    /**
     * Fejlec es a csekk kitoltese
     * @param szamla
     * @throws DocumentException
     * @throws IOException
     */
    public static InputStream fillPdfForm(Szamla szamla) throws DocumentException, IOException{

        InputStream is = FileUtil.findResourceOnClasspath("csekk_template.pdf");
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        PdfReader reader = new PdfReader(is);
        PdfStamper stamper = new PdfStamper(reader, os);

        AcroFields form = stamper.getAcroFields();
        BaseFont unicode = BaseFont.createFont("ARIALUNI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        form.addSubstitutionFont(unicode);
        form.setField("osszeg1", "***" + Long.toString(Math.round(szamla.getBruttoOsszeg())) + "***");
        form.setField("osszeg2", "***" + Long.toString(Math.round(szamla.getBruttoOsszeg())) + "***");
        String szamBetuvel = "*" + NumberUtil.szamBetuvel(szamla.getBruttoOsszeg()) + "*";
        if (szamBetuvel.length() > 34){
            String elsoResz = szamBetuvel.substring(0, szamBetuvel.indexOf('-') + 1);
            String masodikResz = szamBetuvel.substring(szamBetuvel.indexOf('-') + 1);
            form.setField("osszeg1_1_betuvel", elsoResz);
            form.setField("osszeg1_2_betuvel", masodikResz);
            form.setField("osszeg2_1_betuvel", szamBetuvel);
        }else{
            form.setField("osszeg1_1_betuvel", szamBetuvel);
            form.setField("osszeg2_1_betuvel", szamBetuvel);
        }

        //befizetoazonosito
        form.setField("befizetoazonosito", Ugyfel.generateBefizetoAzonosito(szamla.getUgyfelKod()));
        form.setField("ugyfkod", szamla.getUgyfelKod());

        //nev, cim
        form.setField("befizeto_nev", szamla.getNev());
        form.setField("befizeto_nev2", szamla.getNev());
        form.setField("befizeto_varos", szamla.getCim().getVaros());
        form.setField("befizeto_varos2", szamla.getCim().getVaros());
        form.setField("befizeto_cim", szamla.getCim().generateCim());
        form.setField("befizeto_cim2", szamla.getCim().generateCim());
        form.setField("iranyitoszam1", Integer.toString(szamla.getCim().getIranyitoszam()));
        form.setField("iranyitoszam2", Integer.toString(szamla.getCim().getIranyitoszam()));
        String nevCim = szamla.getNev() + "\r\n" + szamla.getCim().getVaros() + " "
                + szamla.getCim().getIranyitoszam() + "\r\n"
                + szamla.getCim().generateCim();
        form.setField("NevCim", nevCim);

        Helper helper = new Helper(szamla);
        form.setField("jogcim1", helper.szamlaszam);
        form.setField("jogcim2", helper.ugyfkod);
        form.setField("jogcim3", helper.tol);
        form.setField("jogcim4", helper.ig);

        form.setField("jogcim_reszletes1", helper.szamlaszam2);
        form.setField("jogcim_reszletes2", helper.ugyfkod2);
        form.setField("jogcim_reszletes3", helper.tolIg2);

        stamper.setFormFlattening(true);
        stamper.close();
        is.close();
        reader.close();

        return new ByteArrayInputStream(os.toByteArray());
    }

    public static class Helper{
        public String szamlaszam;
        public String ugyfkod;
        public String tol;
        public String ig;
        public String tolIg;
        public String szamlaszam2;
        public String ugyfkod2;
        public String tolIg2;
        public String kelt;
        public String teljesites;
        public String hatarido;

        public Helper(Szamla szamla) {
            szamlaszam = szamla.getSzamlaSorszam();
            ugyfkod = szamla.getUgyfelKod();
            tol = DateUtil.formatDate(szamla.getIdoszakKezdete()) + " - " ;
            ig = DateUtil.formatDate(szamla.getIdoszakVege());
            tolIg = tol + ig;
            szamlaszam2 = "Sz\u00e1mlasz\u00e1m: " + szamlaszam;
            ugyfkod2 = "Azonosit\u00f3: " + ugyfkod;
            tolIg2 = "Id\u0151szak: " + tolIg;
            kelt = DateUtil.formatDate(szamla.getKelte());
            teljesites = DateUtil.formatDate(szamla.getTeljesites());
            hatarido = DateUtil.formatDate(szamla.getHatarido());
        }

    }
}