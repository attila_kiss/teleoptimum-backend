package hu.elektra.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import hu.elektra.core.Reszletezo;
import hu.elektra.core.Szamla;

import javax.ws.rs.WebApplicationException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

/**
 * Created by attila on 23/02/17.
 */
public class ReszletezoNyomtato {

    private static BaseFont unicode;
    static {
        try {
            unicode = BaseFont.createFont("ARIALUNI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        } catch (DocumentException | IOException e) {
            throw new WebApplicationException(e);
        }
    }


    public static Document nyomtat(Document document, Szamla szamla) throws DocumentException {
        Phrase phrase = new Phrase();
        phrase.add(szamla.getNev() + " melléklete a " + szamla.getSzamlaSorszam() + " sorszámú számlához:");
        document.add(phrase);
        for (Reszletezo reszletezo: szamla.getTelefonszamok()){
            String formattedtelefonszam = reszletezo.getTelefonszam().replaceAll("(\\d{2})(\\d{3})(\\d{4})", "($1) $2-$3");
            String title = "Telefonszám: " + formattedtelefonszam;
            PdfPTable table = createTableHeader(title, szamla);
            addTableRows(reszletezo.getRows(), table);
            addSumRow(reszletezo.getRows(), table);
            document.add(table);
        }
        return  document;
    }

    public static void addTableRows(Reszletezo.Row[] rows, PdfPTable table){
        DecimalFormat df = new DecimalFormat("#.00");
        for (Reszletezo.Row row : rows) {
            table.addCell(addRow(row.getTermeknev(), Element.ALIGN_LEFT));
            table.addCell(addRow(row.getEgyseg(), Element.ALIGN_LEFT));
            table.addCell(addRow(df.format(row.getMennyiseg()), Element.ALIGN_RIGHT));
            table.addCell(addRow(df.format(row.getNettoegysegar()), Element.ALIGN_RIGHT));
            table.addCell(addRow(df.format(row.getAfakulcs()), Element.ALIGN_RIGHT));
            table.addCell(addRow(df.format(row.getNettoar()), Element.ALIGN_RIGHT));
            table.addCell(addRow(df.format(row.getNettoar() * row.getAfakulcs() / 100), Element.ALIGN_RIGHT));
            table.addCell(addRow(df.format(row.getBruttoar()), Element.ALIGN_RIGHT));
        }
    }

    private static void addSumRow(Reszletezo.Row[] rows, PdfPTable table){
        DecimalFormat df = new DecimalFormat("#.00");
        double osszesen = Arrays.stream(rows).map(Reszletezo.Row::getBruttoar).reduce((aDouble, aDouble2) -> aDouble + aDouble2).get();
        Font f = new Font(unicode);
        f.setSize(7);
        Phrase phrase = new Phrase("Összesen: " + df.format(osszesen), f);
        PdfPCell titleCell = new PdfPCell(phrase);
        titleCell.setBorder(Rectangle.BOTTOM);
        titleCell.setColspan(8);
        titleCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(titleCell);
    }

    private static PdfPTable createTableHeader(String title, Szamla szamla) {

        PdfPTable table = new PdfPTable(new float[] { 4, 1, 1, 1, 1, 1, 1, 1 });
        table.setWidthPercentage(100);
        table.setSpacingBefore(0f);
        table.setSpacingAfter(10f);

        Font f = new Font(unicode);
        f.setSize(9);
        Phrase phrase = new Phrase(title, f);
        PdfPCell titleCell = new PdfPCell(phrase);
        titleCell.setBorder(Rectangle.TOP + Rectangle.LEFT + Rectangle.RIGHT);
        titleCell.setColspan(8);
        table.addCell(titleCell);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        phrase = new Phrase("Időszak: " + formatter.format(szamla.getIdoszakKezdete()) + " - " + formatter.format(szamla.getIdoszakVege()), f);
        titleCell = new PdfPCell(phrase);
        titleCell.setBorder(Rectangle.TOP + Rectangle.LEFT);
        titleCell.setColspan(4);
        table.addCell(titleCell);

        phrase = new Phrase("Ügyfélkód: " + szamla.getUgyfelKod(), f);
        titleCell = new PdfPCell(phrase);
        titleCell.setBorder(Rectangle.TOP + Rectangle.LEFT + Rectangle.RIGHT);
        titleCell.setColspan(4);
        table.addCell(titleCell);

        phrase = new Phrase(" ", f);
        titleCell = new PdfPCell(phrase);
        titleCell.setBorder(Rectangle.TOP);
        titleCell.setColspan(8);
        table.addCell(titleCell);

        table.addCell(addHeader("Terméknév", Element.ALIGN_LEFT));
        table.addCell(addHeader("Egység", Element.ALIGN_LEFT));
        table.addCell(addHeader("Mennyiség", Element.ALIGN_RIGHT));
        table.addCell(addHeader("Egység Ár", Element.ALIGN_RIGHT));
        table.addCell(addHeader("Áfakulcs", Element.ALIGN_RIGHT));
        table.addCell(addHeader("Nettó Ár", Element.ALIGN_RIGHT));
        table.addCell(addHeader("Áfa", Element.ALIGN_RIGHT));
        table.addCell(addHeader("Brutto Ár", Element.ALIGN_RIGHT));
        table.setHeaderRows(1);
        return table;
    }

    private static PdfPCell addHeader(String header, int alignment) {
        Font f = new Font(unicode);
        f.setSize(7);
        f.setStyle("bold");
        PdfPCell cell = new PdfPCell(new Phrase(header, f));
        // cell.setBackgroundColor(Color.LIGHT_GRAY);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(Rectangle.BOTTOM);
        return cell;
    }

    private static PdfPCell addRow(String header, int alignment){
        Font f = new Font(unicode);
        f.setSize(7);
        PdfPCell cell = new PdfPCell(new Phrase(header, f));
        // cell.setBackgroundColor(Color.LIGHT_GRAY);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(Rectangle.BOTTOM);
        return cell;
    }
}
