package hu.elektra.pdf;

import com.itextpdf.text.DocumentException;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import hu.elektra.core.Szamla;
import hu.elektra.core.Ugyfel;
import hu.elektra.db.SzamlaDao;
import hu.elektra.util.Elektra;
import hu.elektra.util.FileUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

public class SzamlazoProgramConnector {

    private final CsekkNyomtato csekkNyomtato;
    private final Elektra elektra;
    public final SzamlaDao szamlaDao;

    public SzamlazoProgramConnector(CsekkNyomtato csekkNyomtato, Elektra elektra, SzamlaDao szamlaDao){
        this.csekkNyomtato = csekkNyomtato;
        this.elektra  = elektra;
        this.szamlaDao = szamlaDao;
    }


    public void generateSzamlak(List<Szamla> szamlak) throws IOException, InterruptedException, DocumentException {
        //read template file
        File tmpDir = new File(elektra.getDataStore() + elektra.getTmpFolder());
        tmpDir.mkdirs();
        Collections.sort(szamlak);
        int szamlaCounter = 0;
        DecimalFormat formatter = new DecimalFormat("0000");
        double szamlazottMennyiseg = 0.0;
        for (Szamla szamla : szamlak) {
            File szamlaFolder = FileUtil.createSzamlaFolder(szamla, elektra);

            //create new text file to write
            String fileName = tmpDir.getAbsolutePath() + "/szamla_" + formatter.format(szamlaCounter) + ".txt";
            //FileWriter outFile = new FileWriter(fileName);
            PrintWriter out = new PrintWriter(fileName, "UTF-8");

            //reading template, and fill with szamla data
            fillTemplateLine(out, szamla);
            out.close();
            final Process p = Runtime.getRuntime().exec(elektra.getSzamlazoPath() + " " + fileName);
            File fileToDelete = new File(fileName + "_2");
            while (!fileToDelete.exists()){
                Thread.sleep(2000);
            }
            p.destroy();
            FileUtils.forceDelete(fileToDelete);
            catchPDF(elektra.getPdfPrintDir(), szamla, szamlaFolder);
            szamlazottMennyiseg += generateCsekk(szamla);
            szamlaDao.updateSzamlakep(szamla);
            System.out.println("Szamla: " +  szamla.getSzamlaSorszam() + " letrehozva");
            szamlaCounter++;
        }
        System.out.println("ellenorzo: " + szamlazottMennyiseg + " Ft");
    }

    private void catchPDF(String pdfFolderPath, Szamla szamla, File szamlaFolder) throws IOException, InterruptedException{
        File pdfDir = new File(pdfFolderPath);
        long t0=System.currentTimeMillis();
        long t1 = t0;
        System.out.println(pdfDir.getAbsolutePath());
        if(pdfDir.listFiles().length < 2){
            while(t1-t0 < 5000){
                t1 = System.currentTimeMillis();
            }
        }
        File[] szamlakepek = pdfDir.listFiles();
        for (File file : szamlakepek) {
            if (file.getName().contains("-1-")){
                String szamlaSorszam = file.getName().substring(0, file.getName().indexOf('-'));
                szamla.setSzamlaSorszam(szamlaSorszam);
                File destFile = new File(szamlaFolder + "/" + file.getName());
                FileUtils.copyFile(file, destFile);
                szamla.setSzamlaKepUrl(FileUtil.convertPathToUri(destFile));
            }
            FileUtils.forceDelete(file);
        }

    }

    private double generateCsekk(Szamla szamla) throws DocumentException, IOException, InterruptedException{
//        File szamlaFolder = FileUtil.createSzamlaFolder(szamla, elektra);
//        File[] filePath = new File[]{szamlaFolder};
//        double csekkOsszeg = csekkNyomtato.fillPdfForm(szamla, filePath);
//        szamla.setCsekkUrl(FileUtil.convertPathToUri(filePath[0]));
//        System.out.println("Csekk: " +  szamla.getSzamlaSorszam() + " osszeg: " + csekkOsszeg);
//        return csekkOsszeg;
        return 0.0;
    }

    private void fillTemplateLine(PrintWriter writer, Szamla szamla){
        writer.println("[FILE_ELEJE]");
        writer.println("[BIZONYLAT_TIPUSA]SZÁMLA");
        writer.println("[_VEVONEVE]" + szamla.getNev());
        writer.println("[VEVOORSZAG]");
        writer.println("[_VEVOIRSZAM]" + szamla.getCim().getIranyitoszam());
        writer.println("[_VEVOTELEPULES]" + szamla.getCim().getVaros());
        writer.println("[_VEVOCIM]" + szamla.getCim().generateCim());
        if (StringUtils.isNotBlank(szamla.getAdoszam())){
            writer.println("[VEVOADOSZAM]" + szamla.getAdoszam());
        }

        if (szamla.getFizetesiMod().equals(Ugyfel.FizetesiMod.POSTAI_CSEKK.name())){
            writer.println("[_FIZETESIMODNEVE]POSTAI CSEKK");
        }else{
            writer.println("[_FIZETESIMODNEVE]ÁTUTALÁS");
        }
        writer.println("[_FIZETESIHATARIDO]" + DateFormatUtils.format(szamla.getHatarido(), "yyyy-MM-dd"));
        writer.println("[_PENZNEM]HUF");
        writer.println("[NYOMTATAS_AZONNAL]IGEN");
        writer.println("[TELJESITES_DATUMA]" + DateFormatUtils.format(szamla.getHatarido(), "yyyy-MM-dd"));
        writer.println("[TERMEKEK_ELEJE]");
        Arrays.stream(szamla.getSzamlaTetelek()).forEach(tetel -> {
            writer.println("[TERMEK]");
            writer.println("[_MEGNEVEZES]" + tetel.getMegnevezes());
            writer.println("[_VTSZSZJ]" + "-");
            writer.println("[_AFAKULCS]" + tetel.getAfakulcs());
            writer.println("[_MERTEKEGYSEG]" + tetel.getEgyseg());
            writer.println("[_MENNYISEG]" + tetel.getMennyiseg());
            writer.println("[_EGYSEGARNETTO]" + tetel.getNettoegysegar());
        });
        writer.println("[TERMEKEK_VEGE]");
        writer.println("[BIZONYLATZARO_SZOVEG]" + "\r\n");
        writer.println("A számla értéke közvetített szolgáltatást tartalmaz!");
        writer.println("\r\n");
        writer.println("Tájékoztatjuk, hogy számlatartozás esetén kimenő hívásai korlátozásra kerülhetnek,");
        writer.println("ezért kérjük figyeljen a számla befizetési határidejére.");
        writer.println("\r\n");
        writer.println("\r\n");
        writer.println(szamla.getUgyfelKod() + "/911100");
        writer.println(".               /467100 (27%)");
        writer.println("[FILE_VEGE]");
    }
}

