package hu.elektra.service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopyFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import hu.elektra.core.Szamla;
import hu.elektra.pdf.CsekkNyomtato;
import hu.elektra.pdf.ReszletezoNyomtato;
import hu.elektra.util.Elektra;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.IOUtils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Log4j
public class PdfRenderService {

    private final Elektra elektra;

    public StreamingOutput mergeSzamlak(List<Szamla> szamlak){
        List<File> szamlaPdf = szamlak.stream().map(szamla -> new File(elektra.getDataStore() + szamla.getSzamlaKepUrl().substring(4,szamla.getSzamlaKepUrl().length())))
                .filter(file -> file.exists()).collect(Collectors.toList());
        return output -> {
            try {
                mergePDF(szamlaPdf, output);
            } catch (Exception e) {
                throw new WebApplicationException(e);
            }
        };
    }

    public StreamingOutput mergeSzamlakWithReszletezo(List<Szamla> szamlak){
        return output -> {
            try {
                PdfCopyFields copy = new PdfCopyFields(output);

                for (Szamla szamla : szamlak){
                    File szamlaPdf = new File(elektra.getDataStore() + szamla.getSzamlaKepUrl().substring(4,szamla.getSzamlaKepUrl().length()));
                    if (!szamlaPdf.exists()){
                        continue;
                    }
                    PdfReader reader = new PdfReader(szamlaPdf.getAbsolutePath());
                    copy.addDocument(reader);

                    //create reszletezo
                    Document document = new Document();
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    PdfWriter.getInstance(document, bos);
                    document.open();
                    ReszletezoNyomtato.nyomtat(document, szamla);
                    //document.newPage();
                    document.close();
                    PdfReader reader1 = new PdfReader(bos.toByteArray());
                    copy.addDocument(reader1);
                    bos.close();
                }
                copy.close();
            } catch (DocumentException e) {
                throw new WebApplicationException(e);
            }
        };

    }

    public StreamingOutput mergeMellekletek(List<Szamla> szamlak){
        return output -> {
            Document document = new Document();
            try {
                PdfWriter.getInstance(document, output);
                document.open();
                for (Szamla szamla : szamlak){
                    ReszletezoNyomtato.nyomtat(document, szamla);
                    document.newPage();
                }
                document.close();
            } catch (DocumentException e) {
                throw new WebApplicationException(e);
            }
        };
    }

    public StreamingOutput mergeCsekkek(List<Szamla> szamlak){
        return output -> {
            try {
                PdfCopyFields copy = new PdfCopyFields(output);
                szamlak.stream().forEach(szamla -> {
                    try {
                        InputStream csekk = CsekkNyomtato.fillPdfForm(szamla);
                        PdfReader reader = new PdfReader(csekk);
                        copy.addDocument(reader);
                    } catch (DocumentException | IOException e) {
                        e.printStackTrace();
                        throw new WebApplicationException(e);
                    }

                });
                copy.close();
            } catch (Exception e) {
                throw new WebApplicationException(e);
            }
        };
    }

    private void mergePDF(final List<File> pages, final OutputStream os) throws IOException, DocumentException{
        if (pages.size() > 1){
            PdfCopyFields copy = new PdfCopyFields(os);
            for (File file : pages) {
                PdfReader reader = new PdfReader(file.getAbsolutePath());
                copy.addDocument(reader);
            }
            copy.close();
        }else{
            IOUtils.copy(new FileReader(pages.get(0)), os);
        }
    }
}
