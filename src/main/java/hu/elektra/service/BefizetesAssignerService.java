package hu.elektra.service;

import hu.elektra.core.Bank;
import hu.elektra.core.Szamla;
import hu.elektra.core.UgyfelSzamlak;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by attila on 12/06/17.
 */
public class BefizetesAssignerService {

    public void assignBefizetesToUgyfel(List<Bank> befizetesek, List<UgyfelSzamlak> ugyfelSzamlak){
        befizetesek.forEach(bank -> {
            Optional<UgyfelSzamlak> optUgyfel = ugyfelSzamlak.stream()
                    .filter(ugyfelSzamla -> {
                        String regex = "(";
                        if (StringUtils.isNotBlank(ugyfelSzamla.getSzamlaSorszamok())){
                            regex += ugyfelSzamla.getSzamlaSorszamok().replace(",", "|") + "|";
                        }
                        regex += ugyfelSzamla.getNev().replace(".", "") + "|" + ugyfelSzamla.getUgyfelKod() + ")";
                        Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE | Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
                        Matcher matcher = pattern.matcher(bank.getLeiras());
                        return matcher.find();
                    }).findFirst();
            optUgyfel.ifPresent(ugyfelSzamla -> {
                ugyfelSzamlak.add(ugyfelSzamlak.remove(ugyfelSzamlak.indexOf(ugyfelSzamla)));
                bank.setUgyfelKod(ugyfelSzamla.getUgyfelKod());
                String[] szamlaSorszamok = ugyfelSzamla.getSzamlaSorszamok().split(",");
                List<String> sorszamok = Arrays.stream(szamlaSorszamok).map(this::getTrimmedSorszam)
                        .filter(s1 -> s1[1] != null && bank.getLeiras().contains(s1[1]))
                        .map(strings -> strings[0])
                        .collect(Collectors.toList());
                bank.setSzamlaSorszamok(sorszamok.toArray(new String[sorszamok.size()]));
            });
        });
    }

    private String[] getTrimmedSorszam(String sorszam){
        String[] ret = new String[2];
        ret[0] = sorszam;
        Pattern pattern = Pattern.compile("P(0+)(\\d+)");
        Matcher matcher = pattern.matcher(sorszam);
        if(matcher.find()){
            ret[1] = matcher.group(2);
        }
        return ret;
    }

    public void assignSzamlaBasedOnValue(Bank bank, List<Szamla> szamlak){
        Optional<String> szamlaSorszam = szamlak.stream().filter(szamla -> {
            double brutto = Math.round(Arrays.stream(szamla.getSzamlaTetelek())
                    .map(tetel -> tetel.getBruttoar())
                    .reduce((aDouble, aDouble2) -> aDouble + aDouble2)
                    .get());
            if (bank.getOsszeg().equals(brutto)){
                return true;
            }else{
                return false;
            }

        }).map(szamla -> szamla.getSzamlaSorszam()).findFirst();
        szamlaSorszam.ifPresent(s -> bank.setSzamlaSorszamok(new String[] {s}));
    }
}
