package hu.elektra.service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import hu.elektra.core.Szamla;
import hu.elektra.db.SzamlaDao;
import hu.elektra.pdf.ReszletezoNyomtato;
import hu.elektra.util.Elektra;
import hu.elektra.util.MailSettings;
import hu.elektra.util.StringUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import javax.mail.util.ByteArrayDataSource;

public class EmailSenderService {

    private Properties properties = System.getProperties();
    private String from = "agnatus@agnatus.hu";

    private final Elektra elektra;
    private final MailSettings mailSettings;

    public EmailSenderService(Elektra elektra, MailSettings mailSettings){
        this.elektra = elektra;
        this.mailSettings = mailSettings;
        properties.setProperty("mail.smtp.auth", "true");
        properties.put("mail.smtp.socketFactory.port", Integer.toString(mailSettings.getPort()));
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.smtp.host", mailSettings.getHost());
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", Integer.toString(mailSettings.getPort()));
    }

    public Session createSession(){
        return Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(mailSettings.getUsername(), mailSettings.getPassword());
                    }
                });
    }

    public Map<String, String> send(Szamla szamla, Session session) {

        String to = szamla.getEmail();
        HashMap<String, String> resp = new HashMap<>();
        resp.put("email", to);
        resp.put("ugyfel", szamla.getNev());
        if (!StringUtil.isNotBlank(to)){
            resp.put("success", "failed");
            return resp;
        }

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("Teleoptimum " + szamla.getSzamlaSorszam() + " számla");

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            // Fill the message
            messageBodyPart.setText("Tisztelt Ügyfelünk!\r\nMellékelten küldjük a számláját a(z) " +
                            formatter.format(szamla.getIdoszakKezdete()) + " - " + formatter.format(szamla.getIdoszakVege()) + " időszakról.");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();


            DataSource source = new ByteArrayDataSource(createAttachment(szamla).toByteArray(), "application/pdf");
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName("szamla.pdf");
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully to " + to + "....");
            resp.put("status", "success");
        } catch (MessagingException | DocumentException | IOException ex) {
            resp.put("status", ex.getMessage());
            ex.printStackTrace();
        }
        return resp;
    }

    private ByteArrayOutputStream createAttachment(Szamla szamla) throws DocumentException, IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        Document reszletezoDocument = new Document();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PdfWriter.getInstance(reszletezoDocument, bos);
        reszletezoDocument.open();
        ReszletezoNyomtato.nyomtat(reszletezoDocument, szamla);
        reszletezoDocument.newPage();
        reszletezoDocument.close();

        PdfReader reszletezoReader = new PdfReader(bos.toByteArray());

        File szamlaKep = new File(elektra.getDataStore() + szamla.getSzamlaKepUrl().substring(4,szamla.getSzamlaKepUrl().length()));
        PdfReader reader = new PdfReader(szamlaKep.getAbsolutePath());
        Document document = new Document();
        PdfCopy copy = new PdfCopy(document, output);
        document.open();

        for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            copy.addPage(copy.getImportedPage(reader, i));
        }

        for (int i = 1; i <= reszletezoReader.getNumberOfPages(); i++) {
            copy.addPage(copy.getImportedPage(reszletezoReader, i));
        }
        document.close();
        reader.close();
        reszletezoReader.close();

        return output;
    }
}
