package hu.elektra.service;

import hu.elektra.core.Bank;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by attila on 24/05/17.
 */
public class BankReaderService {

    public List<Bank> readExcel(InputStream is) throws IOException {
        List<Bank> bankEntries = new ArrayList<>();
        Workbook workbook = new HSSFWorkbook(is);
        Sheet firstSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = firstSheet.iterator();
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            if (nextRow.getRowNum() < 9){
                continue;
            }
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            Bank.BankBuilder bankBuilder = Bank.builder();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (cell.getColumnIndex() == 0){
                    bankBuilder.datum(cell.getDateCellValue());
                }else if (cell.getColumnIndex() == 1){
                    bankBuilder.leiras(cell.getStringCellValue());
                }else if (cell.getColumnIndex() == 2){
                    bankBuilder.osszeg(cell.getNumericCellValue());
                }
            }
            bankEntries.add(bankBuilder.build());
        }
        workbook.close();
        is.close();
        return bankEntries;
    }
}
