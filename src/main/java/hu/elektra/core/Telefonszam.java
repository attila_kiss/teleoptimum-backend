package hu.elektra.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import javax.validation.constraints.NotNull;
import java.sql.ResultSet;
import java.sql.SQLException;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode(of = {"telefonszam"})
@ToString
public class Telefonszam {

    @NotNull
    @JsonProperty
    private Long id;

    //private Integer version;

    @NotNull
    @JsonProperty
    private String telefonszam;

    private long ugyfelId;

    //@Convert(converter = SajatTarifaConverterJson.class)
    //private String tarifa;

    public static class TelefonszamMapper implements ResultSetMapper<Telefonszam> {

        @Override
        public Telefonszam map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
            return new Telefonszam(resultSet.getLong("TELEFONSZAM_ID"), resultSet.getString("TELEFONSZAM"), resultSet.getLong("UGYFEL_ID"));
        }
    }
}
