package hu.elektra.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Cim implements Serializable {

    @NotNull
    @JsonProperty("orszag")
    private String orszag = "Magyarország";

    @NotNull
    @JsonProperty("varos")
    private String varos;

    @NotNull
    @JsonProperty("kozterulet")
    private String kozterulet;

    @NotNull
    @JsonProperty("kozterulet_tipus")
    private String kozteruletTipus = "";

    @JsonProperty("haz_szam")
    private String hazSzam;

    @JsonProperty("emelet")
    private String emelet;

    @JsonProperty("ajto")
    private String ajto;

    @NotNull
    @JsonProperty("iranyitoszam")
    private int iranyitoszam;

    @NotNull
    @JsonProperty("tipus")
    private CimTipus tipus = CimTipus.ALLANDO;

    public Cim() {
        super();
    }

    // Copy constructor
    public Cim(Cim cim) {
        this(cim.varos, cim.kozterulet, cim.kozteruletTipus, cim.hazSzam, cim.emelet, cim.ajto, cim.iranyitoszam, cim.tipus);
    }

    public static Cim newCopyInstance(Cim cim) {
        return new Cim(cim.varos, cim.kozterulet, cim.kozteruletTipus, cim.hazSzam, cim.emelet, cim.ajto, cim.iranyitoszam, cim.tipus);
    }

    public Cim(String varos, String kozterulet, String kozteruletTipus, String hazSzam, String emelet, String ajto, int iranyitoszam, CimTipus tipus) {
        this.varos = varos;
        this.kozterulet = kozterulet;
        this.kozteruletTipus = kozteruletTipus;
        this.hazSzam = hazSzam;
        this.emelet = emelet;
        this.ajto = ajto;
        this.iranyitoszam = iranyitoszam;
        this.tipus = tipus;
    }

    public String generateCim() {
        StringBuilder cim = new StringBuilder();
        cim.append(kozterulet).append(" ");
        cim.append(KozteruletTipus.findKozteruletTipus(kozteruletTipus)).append(" ");
        if (StringUtils.isNotBlank(hazSzam)) {
            cim.append(hazSzam).append(hazSzam.endsWith(".") ? " " : ". ");
            if (StringUtils.isNotBlank(emelet)) {
                cim.append(emelet).append(emelet.endsWith(".") ? " " : ". ");
                if (StringUtils.isNotBlank(ajto)) {
                    cim.append(ajto);
                }
            }
        }
        return cim.toString();
    }
}