package hu.elektra.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.elektra.db.UgyfelDao;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown=true)
public class Bank {

    private Date datum;
    private String leiras;
    private Double osszeg;
    private String ugyfelKod;
    private String[] szamlaSorszamok;


    public static class BankMapper implements ResultSetMapper<Bank>{

        @Override
        public Bank map(int index, ResultSet r, StatementContext ctx) throws SQLException {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, false);
            Bank bank = Bank.builder()
                    .datum(r.getDate("datum"))
                    .leiras(r.getString("leiras"))
                    .osszeg(r.getDouble("osszeg"))
                    .ugyfelKod(r.getString("ugyfel_kod"))
                    .build();
            try {
                String szamlaSorszamok = r.getString("szamla_sorszam");
                if (StringUtils.isNoneBlank(szamlaSorszamok)){
                    bank.szamlaSorszamok = mapper.readValue(r.getString("szamla_sorszam"), String[].class);
                }
            } catch (IOException e) {
                throw new SQLException(e);
            }
            return bank;
        }
    }
}
