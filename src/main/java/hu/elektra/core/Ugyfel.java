package hu.elektra.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown=true)
public class Ugyfel {

    public enum FizetesiMod {
        POSTAI_CSEKK("postai csekk"),
        ATUTALAS("átutalás");

        private final String nev;

        FizetesiMod(String nev) {
            this.nev = nev;
        }

        public String getNev() {
            return nev;
        }
    }

    private Long id;
    private String nev;
    private String ugyfelKod;
    private String fizetesiMod;
    private String szamlaSzam;
    private String email;
    private String adoszam;
    private Boolean szamlaEmailben;
    private String ugyfelTipus;
    private String ceg_jegyzek_szam;
    private String kacsolat_tarto_nev;
    private String kapcsolat_tarto_sz_ig_szam;
    private String kapcsolat_tarto_tsz;
    private String anyja_neve;
    private String egyeb_telefonszam;
    private String sz_hely_ido;
    private String szem_ig_szam;

    public String getBefizetoAzonosito(){
        return generateBefizetoAzonosito();
    }


    /**
     * jelenleg az L-W-B -nek kell csak hogy mas legyen a szamlan
     * mint a tobbieknel
     * null vagy 0 : normal szamla
     * 1 : nem kell szamla
     * 2 : specialis szamla kell
     * 3 : Minden megy at valtozatlanul egy az egyben
     */
    private Short needsSpecialInvoice = 0;

    private Set<Telefonszam> telefonszamok;
    private Cim[] cimek;
    //private Set<SajatTarifa> defaultTarifak;

    public String generateBefizetoAzonosito(){
        return generateBefizetoAzonosito(this.ugyfelKod);
    }

    public static String generateBefizetoAzonosito(String ugyfelKod){
        int check = 0;
        char[] kod = ("70" + ugyfelKod).toCharArray();
        boolean mindenMasodik = true;
        for (int j = kod.length - 1; j >=0; j--){
            Character c = kod[j];
            if (mindenMasodik){
                int temp = Integer.parseInt(c.toString()) * 2;
                if (temp > 10){
                    check += 1 + (temp - 10);
                }else{
                    check += temp;
                }
            }else{
                check +=Integer.parseInt(c.toString());
            }
            if (mindenMasodik){
                mindenMasodik = false;
            }else{
                mindenMasodik = true;
            }
        }
        int kulonbseg = 0;
        if (check % 10 != 0){
            kulonbseg = 10 - (check % 10);
        }
        return ("70" + ugyfelKod) + kulonbseg;
    }

    public static class UgyfelMapper implements ResultSetMapper<Ugyfel> {

        @Override
        public Ugyfel map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, false);
            Ugyfel ugyfel = Ugyfel.builder().ugyfelKod(resultSet.getString("ugyfel_kod"))
                    .email(resultSet.getString("email"))
                    .fizetesiMod(resultSet.getString("fizetesi_mod"))
                    .id(resultSet.getLong("ugyfel_id"))
                    .needsSpecialInvoice(resultSet.getShort("needs_special_invoice"))
                    .nev(resultSet.getString("nev"))
                    .szamlaEmailben(resultSet.getBoolean("szamla_emailben"))
                    .szamlaSzam(resultSet.getString("szamla_szam"))
                    .adoszam(resultSet.getString("adoszam"))
                    .ceg_jegyzek_szam(resultSet.getString("ceg_jegyzek_szam"))
                    .kacsolat_tarto_nev(resultSet.getString("kacsolat_tarto_nev"))
                    .kapcsolat_tarto_sz_ig_szam(resultSet.getString("kapcsolat_tarto_sz_ig_szam"))
                    .kapcsolat_tarto_tsz(resultSet.getString("kapcsolat_tarto_tsz"))
                    .anyja_neve(resultSet.getString("anyja_neve"))
                    .egyeb_telefonszam(resultSet.getString("egyeb_telefonszam"))
                    .sz_hely_ido(resultSet.getString("sz_hely_ido"))
                    .szem_ig_szam(resultSet.getString("szem_ig_szam"))
                    .ugyfelTipus(resultSet.getString("ugyfel_tipus"))
                    .telefonszamok(new HashSet<>()).build();
            try {
                Cim[] cimek = mapper.readValue(resultSet.getString("cimek"), Cim[].class);
                ugyfel.cimek = cimek;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return ugyfel;
        }
    }
}
