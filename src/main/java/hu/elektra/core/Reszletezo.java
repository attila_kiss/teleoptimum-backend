package hu.elektra.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown=true)
public class Reszletezo {

    private String id;
    private String telefonszam;
    private int ugyfelId;
    Row[] rows;

    @Getter
    @Setter
    @Builder
    @AllArgsConstructor(access = AccessLevel.PUBLIC)
    @ToString
    @EqualsAndHashCode
    @JsonIgnoreProperties(ignoreUnknown=true)
    public static class Row{
        private String id;
        private String telefonszam;
        private String termeknev;
        private double mennyiseg;
        private String egyseg;
        private double nettoar;
        private double bruttoar;
        private String tipus;
        private int afakulcs;
        private double nettoegysegar;
        private String szamlaTipus;
        private boolean tovabbszamlazva;
        private Double afa;
        private String tipus2;
    }
}
