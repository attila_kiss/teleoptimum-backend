package hu.elektra.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class SajatTarifa {

    @NotNull
    private String tipus;

    @NotNull
    private Double afa;

    @NotNull
    @JsonProperty("egyseg_ar")
    private Double egysegAr;

    public void updateTarifa(SajatTarifa otherTarifa){
        if (this.tipus.equals(otherTarifa.getTipus())){
            this.afa = otherTarifa.getAfa();
            this.egysegAr = otherTarifa.getEgysegAr();
        }else{
            throw new RuntimeException("Nem egyforma tarifa tipus");
        }
    }

}