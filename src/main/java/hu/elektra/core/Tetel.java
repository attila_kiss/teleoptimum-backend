package hu.elektra.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown=true)
public class Tetel implements Serializable {

    private String telefonszam;
    private String megnevezes;
    private String egyseg;
    private int mennyiseg;
    private double nettoegysegar;
    private double nettoar;
    private double bruttoar;
    private int afakulcs;
    private String tipus;
}
