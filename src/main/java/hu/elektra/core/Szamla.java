/**
 * CREATE TABLE `new_teleoptimum`.`szamla` (
 * `szamla_id` BIGINT(20) NOT NULL,
 * `nev` VARCHAR(255) NOT NULL,
 * `cim` VARCHAR(255) NOT NULL,
 * `adoszam` VARCHAR(100) NULL,
 * `fizetesi_mod` VARCHAR(45) NOT NULL,
 * `szamla_kelte` DATE NOT NULL,
 * `fizetesi_hatarido` DATE NOT NULL,
 * `teljesites` DATE NOT NULL,
 * `szamlazasi_idoszak_kezdete` DATE NOT NULL,
 * `szamlazasi_idoszak_vege` DATE NOT NULL,
 * `szamla_tetelek` TEXT NULL,
 * `reszeltezo_tetelek` TEXT NULL,
 * `szamla_kep_url` VARCHAR(255) NULL,
 * `csekk_url` VARCHAR(255) NULL,
 * `ugyfel_kod` VARCHAR(255) NULL,
 * PRIMARY KEY (`szamla_id`));
 **/

package hu.elektra.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import lombok.extern.log4j.Log4j;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
@EqualsAndHashCode
@Log4j
@JsonIgnoreProperties(ignoreUnknown=true)
public class Szamla implements Comparable<Szamla> {

    private long id;

    String nev;
    String adoszam;
    String fizetesiMod;
    String ugyfelKod;
    String email;

    private Date kelte;
    private Date hatarido;
    private Date teljesites;

    private Date idoszakKezdete;
    private Date idoszakVege;

    private Cim cim;
    private Tetel[] szamlaTetelek;
    private Reszletezo[] telefonszamok;

    private String szamlaSorszam;

    private String szamlaKepUrl;
    private String csekkUrl;

    private String telenorSzamlaSorszam;

    public double getBruttoOsszeg(){
        return Arrays.stream(this.szamlaTetelek).map(tetel -> tetel.getBruttoar()).reduce((aDouble, aDouble2) -> aDouble + aDouble2).get();
    }

    @Override
    public int compareTo(Szamla szamla) {
        return this.getNev().compareTo(szamla.getNev());
    }

    public static class SzamlaMapper implements ResultSetMapper<Szamla>{

        @Override
        public Szamla map(int i, ResultSet rs, StatementContext statementContext) throws SQLException {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, false);
            Szamla szamla = Szamla.builder()
                    .id(rs.getLong("szamla_id"))
                    .nev(rs.getString("nev"))
                    .adoszam(rs.getString("adoszam"))
                    .email(rs.getString("email"))
                    .fizetesiMod(rs.getString("fizetesi_mod"))
                    .kelte(rs.getDate("kelte"))
                    .hatarido(rs.getDate("hatarido"))
                    .teljesites(rs.getDate("teljesites"))
                    .idoszakKezdete(rs.getDate("idoszak_kezdete"))
                    .idoszakVege(rs.getDate("idoszak_vege"))
                    .szamlaKepUrl(rs.getString("szamla_kep_url"))
                    .csekkUrl(rs.getString("csekk_url"))
                    .ugyfelKod(rs.getString("ugyfel_kod"))
                    .szamlaSorszam(rs.getString("szamla_sorszam"))
                    .telenorSzamlaSorszam(rs.getString("telenor_szamla_sorszam"))
                    .build();
            try {
                Tetel[] szamlaTetelek = mapper.readValue(rs.getString("szamla_tetelek"), Tetel[].class);
                Reszletezo[] reszletezoTetelek = mapper.readValue(rs.getString("reszletezo_tetelek"), Reszletezo[].class);
                Cim cim = mapper.readValue(rs.getString("cim"), Cim.class);
                szamla.telefonszamok = reszletezoTetelek;
                szamla.szamlaTetelek = szamlaTetelek;
                szamla.cim = cim;
                return szamla;
            } catch (IOException e) {
                log.error("failed tp parse tetelek", e);

            }
            return szamla;
        }
    }
}
