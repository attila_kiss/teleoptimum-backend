package hu.elektra.core;

import lombok.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
@EqualsAndHashCode
public class UgyfelSzamlak {
    Long ugyfelId;
    String ugyfelKod;
    String nev;
    String szamlaSorszamok = "";

    public static class UgyfelSzamlakMapper implements ResultSetMapper<UgyfelSzamlak> {

        @Override
        public UgyfelSzamlak map(int index, ResultSet r, StatementContext ctx) throws SQLException {
            return UgyfelSzamlak.builder()
                    .ugyfelId(r.getLong("ugyfel_id"))
                    .ugyfelKod(r.getString("ugyfel_kod"))
                    .nev(r.getString("nev"))
                    .szamlaSorszamok(r.getString("szamlak"))
                    .build();
        }
    }
}
