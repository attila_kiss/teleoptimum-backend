package hu.elektra.core;

public enum KozteruletTipus {
    utca("utca"),
    ut("út"),
    ter("tér"),
    korut("körút"),
    erdosor("erdősor"),
    koz("köz"),
    lakotelep("lakótelep"),
    lejto("lejtő"),
    park("park"),
    puszta("puszta"),
    setany("sétány"),
    sor("sor"),
    sugarut("sugárút"),
    pf("pf."),
    hrsz("hrsz."),
    hatarozatlan("");

    private final String nev;

    KozteruletTipus(String nev) {
        this.nev = nev;
    }

    public String nev() {
        return nev;
    }

    public static String findKozteruletTipus(String kozterulet){
        for (int i=0; i < KozteruletTipus.values().length; i++){
            if (KozteruletTipus.values()[i].name().equals(kozterulet)){
                return KozteruletTipus.values()[i].nev;
            }
        }
        return kozterulet;
    }
}
