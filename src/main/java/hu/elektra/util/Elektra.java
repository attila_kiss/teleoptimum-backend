package hu.elektra.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Elektra {

    @JsonProperty
    private String dataStore;

    @JsonProperty
    private String ghostScript;

    @JsonProperty
    private String szamlazoPath;

    @JsonProperty
    private String pdfPrintDir;

    @JsonProperty
    private String szamlaTemplate;

    @JsonProperty
    private String tmpFolder;
}
