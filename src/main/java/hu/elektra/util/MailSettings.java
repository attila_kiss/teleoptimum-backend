package hu.elektra.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MailSettings {

    private String host;
    private int port;
    private String username;
    private String password;
}
