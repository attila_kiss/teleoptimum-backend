package hu.elektra.util;

public class StringUtil {
    public static boolean isNotBlank(String myString) {
        if (myString.trim().equals("NULL")) {
            myString = "";
        }
        return org.apache.commons.lang3.StringUtils.isNotBlank(myString);
    }

    public static String formatTelefonszam(String telefonszam) {

        Object[] teljes = new String[4];
        teljes[0] = "+36";
        teljes[1] = telefonszam.substring(0, 2);
        teljes[2] = telefonszam.substring(2, 5);
        teljes[3] = telefonszam.substring(5);
        return String.format("%1s(%2s) %3s-%4s", teljes);
    }

    public static String removeEkezet(String s) {
        StringBuffer sb = new StringBuffer();
        char[] c = s.toCharArray();
        for (int i = 0; i < c.length; i++)
            switch (c[i]) {
                case 'á':
                    sb.append("a");
                    break;
                case 'Á':
                    sb.append("A");
                    break;
                case 'é':
                    sb.append("e");
                    break;
                case 'É':
                    sb.append("E");
                    break;
                case 'í':
                    sb.append("i");
                    break;
                case 'Í':
                    sb.append("I");
                    break;
                case 'ö':
                    sb.append("o");
                    break;
                case 'Ö':
                    sb.append("O");
                    break;
                case 'ő':
                    sb.append("o");
                    break;
                case 'Ő':
                    sb.append("O");
                    break;
                case 'ó':
                    sb.append("o");
                    break;
                case 'Ó':
                    sb.append("O");
                    break;
                case 'ú':
                    sb.append("u");
                    break;
                case 'Ú':
                    sb.append("u");
                    break;
                case 'ü':
                    sb.append("u");
                    break;
                case 'Ü':
                    sb.append("u");
                    break;
                default:
                    sb.append(c[i]);
            }
        return sb.toString();
    }

}
