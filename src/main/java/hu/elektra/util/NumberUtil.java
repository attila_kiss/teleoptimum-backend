package hu.elektra.util;

        import java.text.DecimalFormat;
        import java.util.Locale;

        import org.apache.commons.lang3.StringUtils;
        import org.apache.commons.lang3.text.WordUtils;
        import org.numbertext.Numbertext;

        import static org.apache.commons.lang3.math.NumberUtils.*;

public class NumberUtil {

    public static Double parseHungarianDouble(String number) {
        if (StringUtils.isNotBlank(number)) {
            String decimal = number.replace(',', '.');
            if (isNumber(decimal)) {
                return createDouble(decimal);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static String szamBetuvel(Double szam) {
        if (szam == null) {
            return "nulla";
        }
        Long lSzam = Math.round(szam);
        return WordUtils.capitalize(Numbertext.numbertext(lSzam.toString(), "hu_HU"));
    }

    public static String szamBetuvel(Integer szam) {
        return szamBetuvel(szam.doubleValue());
    }

    public static Double roundTwoDecimals(Double d) {
        DecimalFormat twoDForm = (DecimalFormat) DecimalFormat.getInstance(Locale.ENGLISH);
        twoDForm.applyPattern("#.##");
        return Double.valueOf(twoDForm.format(d));
    }

    public static String getOnlyNumerics(String str) {

        if (str == null) {
            return null;
        }

        StringBuffer strBuff = new StringBuffer();
        char c;

        for (int i = 0; i < str.length() ; i++) {
            c = str.charAt(i);

            if (Character.isDigit(c)) {
                strBuff.append(c);
            }
        }
        return strBuff.toString();
    }

    public static double roundToDecimals(double value, int decimals) {
        long temp= Math.round((value*Math.pow(10,decimals)));
        return (((double)temp)/Math.pow(10,decimals));
    }
}