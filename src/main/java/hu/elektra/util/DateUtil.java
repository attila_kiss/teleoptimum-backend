package hu.elektra.util;

import org.apache.commons.lang3.time.DateUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DateUtil {
    public static Date maxDate(){
        try {
            return DateUtils.parseDate("9999-12-31", new String[]{"yyyy-MM-dd"});
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static Date getDatePart(Date datum){
        Calendar cal = Calendar.getInstance();
        cal.setTime(datum);

        // Set time fields to zero
        resetTime(cal);

        // Put it back in the Date object
        return cal.getTime();
    }

    public static Date getCurrrentNap(){
        return getDatePart(new Date());
    }

    public static Date countSzamlazasiIdoszakKezdete(Date szamlazasiIdoszakVege){
        Calendar cal = Calendar.getInstance();
        cal.setTime(szamlazasiIdoszakVege);
        resetTime(cal);
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1);

        return cal.getTime();
    }

    public static Date getSzamlazasiIdoszakKezdete(Date datum){
        Calendar cal = Calendar.getInstance();
        cal.setTime(datum);
        resetTime(cal);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        if (day < 12){
            month--;
        }
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, 12);

        return cal.getTime();
    }

    /**
     * Format date to yyyy.MM.dd
     * @param date
     * @return
     */
    public static String formatDate(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd.");
        return formatter.format(date);
    }

    /**
     * Format date time part to hh:mm:ss
     * @param date
     * @return
     */
    public static String formatTime(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        return formatter.format(date);
    }

    /**
     * Format date to yyyyMMdd as a folder name
     * @param date
     * @return
     */
    public static String formatFolderDate(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        return formatter.format(date);
    }

    public static double convertToSeconds(String time) throws DateTimeParseException{
        String[] parts = time.split(":");
        if (parts.length != 3){
            throw new DateTimeParseException("Nem megfelelo ido formatum: hh:mm:ss", time, 0);
        }
        int seconds = 0;
        seconds += Integer.parseInt(parts[0]) * 60*60;
        seconds += Integer.parseInt(parts[1]) * 60;
        seconds += Integer.parseInt(parts[2]);
        return seconds;
    }

    public static float convertToMinutes(String time) throws DateTimeParseException{
        String[] parts = time.split(":");
        if (parts.length != 4){
            Pattern splitter = Pattern.compile("..");
            Matcher myMatcher = splitter.matcher(time);
            ArrayList<String> allMatches = new ArrayList<String>();
            while(myMatcher.find()) {
                allMatches.add(myMatcher.group());
            }
            parts = allMatches.toArray(new String[allMatches.size()]);
            if (parts.length != 4){
                throw new DateTimeParseException("Nem megfelelo ido formatum: hh:mm:ss", time, 0);
            }
        }
        float minutes = 0;
        minutes += Float.parseFloat(parts[1]) * 60;
        minutes += Float.parseFloat(parts[2]);
        minutes += Float.parseFloat(parts[3] + ".0") / 60;
        return minutes;
    }

    public static String formatSeconds(double seconds){
        int hours = (int)(seconds / 3600);
        int minutes = (int)((seconds - (hours * 3600)) / 60);
        int second = (int)(seconds - (hours * 3600) - (minutes * 60));
        DecimalFormat f = new DecimalFormat("00");
        return f.format(hours) + ":" + f.format(minutes) + ":" + f.format(second);
    }

    public static String formatMinutes(float timeInMinutes){
        int hours = (int)Math.floor(timeInMinutes / 60);
        int minutes = (int)Math.floor(timeInMinutes - (hours * 60));
        int seconds = (int)(timeInMinutes - (hours * 60) - minutes) * 60;
        DecimalFormat f = new DecimalFormat("00");
        return f.format(hours) + ":" + f.format(minutes) + ":" + f.format(seconds);
    }

    public static boolean isSameMonth(Date dateEnd, Date date2){
        Date dateStart = DateUtils.addDays(dateEnd, -31);

        return (dateStart.before(date2) && dateEnd.after(date2));
    }

    private static void resetTime(Calendar cal){
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
    }
}