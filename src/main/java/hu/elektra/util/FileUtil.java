package hu.elektra.util;

        import hu.elektra.core.Szamla;
        import hu.elektra.pdf.CsekkNyomtato;

        import java.io.*;
        import java.net.URL;
        import java.util.List;

public class FileUtil {

    public static final String szamlaFolder = "\\szamlak\\";

    public static File findFileOnClasspath(String fileName){
        URL config = FileUtil.class.getClassLoader().getResource(fileName);
        File configFile = null;
        System.out.println("config: " + config);
        if (config != null){
            configFile = new File(config.getFile());
        }else{
            configFile = new File(fileName);
        }
        System.out.println(configFile.getAbsolutePath());
        return configFile.exists() ? configFile : null;
    }

    public static InputStream findResourceOnClasspath(String fileName) throws FileNotFoundException {
        InputStream in = FileUtil.class.getClassLoader().getResourceAsStream(fileName);
        if (in != null){
            return in;
        }else{
            File configFile = new File(fileName);
            return configFile.exists() ? new FileInputStream(configFile) : null;
        }
    }


    public static boolean recursiveDelete(File dir){
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = recursiveDelete(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

    public static boolean emptyDirectory(File dir){
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = emptyDirectory(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String convertPathToUri(File file){
        String path = file.getAbsolutePath();
        path = path.replaceAll("\\\\", "/");
        path = path.substring(path.indexOf("data"), path.length());
        return path;
    }

    public static File convertUriToRealPath(String uri, Elektra elektra){
        uri = uri.replaceAll("/", "\\" + File.separator);
        String physicalPath = elektra.getDataStore() + uri.substring(uri.indexOf("data") + 4, uri.length());
        File file = new File(physicalPath);
        if (file.exists()){
            return file;
        }else{
            throw new RuntimeException("Nem letezik a file: " + physicalPath);
        }
    }

    public static String convertPathToRelative(String path){
        if (path.startsWith("/")){
            return path.substring(1, path.length());
        }else{
            return path;
        }
    }

    public static File createSzamlaFolder(Szamla szamla, Elektra elektra){
        File dir = new File(elektra.getDataStore() + szamlaFolder + DateUtil.formatFolderDate(szamla.getIdoszakVege()) + "\\" + szamla.getUgyfelKod());
        dir.mkdirs();
        return dir;
    }

    public static File mergePDF(List<File> pages, String fileName, Elektra elektra) throws IOException{
        return mergePDF(pages, fileName, null, elektra);
    }

    public static File mergePDF(List<File> pages, String fileName, String destPath, Elektra elektra) throws IOException {
        String helper = "";
        String path = destPath;
        for (File file : pages) {
            helper += file.getAbsolutePath() + " ";
            if (path == null) {
                path = file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(file.getName()));
            }
        }
        if (!path.endsWith(File.separator)){
            path += File.separator;
        }
        String command = elektra.getGhostScript() + " -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=" + path + fileName + " -dBATCH " + helper;
        System.out.println(command);
        Process p = Runtime.getRuntime().exec(command);
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line = null;
        while ((line = in.readLine()) != null) {
            System.out.println(line);
        }
        return new File(path + fileName);
    }
}
