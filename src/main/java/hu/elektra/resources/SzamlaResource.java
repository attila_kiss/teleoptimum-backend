package hu.elektra.resources;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfCopyFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import hu.elektra.core.Szamla;
import hu.elektra.db.SzamlaDao;
import hu.elektra.pdf.CsekkNyomtato;
import hu.elektra.pdf.ReszletezoNyomtato;
import hu.elektra.pdf.SzamlazoProgramConnector;
import hu.elektra.service.EmailSenderService;
import hu.elektra.service.PdfRenderService;
import hu.elektra.util.Elektra;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.Query;

import javax.annotation.security.PermitAll;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Path("/szamlak")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Log4j
@PermitAll
public class SzamlaResource {

    private final SzamlaDao szamlaDao;
    private final SzamlazoProgramConnector szamlazoProgramConnector;
    private final Elektra elektra;
    private final EmailSenderService emailSenderService;
    private final DBI jdbi;
    private final PdfRenderService pdfRenderService;

    @GET
    public List<Szamla> listSzamla(@QueryParam("from") Optional<Long> from, @QueryParam("to") Optional<Long> to) throws ParseException {
        if (from.isPresent() && to.isPresent()){
            return szamlaDao.findSzamlaByKelteBetween(new Date(from.get()), new Date(to.get()));
        }else{
            return szamlaDao.list();
        }
    }

    @POST
    public List<Szamla> bulkInsertSzamlak(List<Szamla> szamlak){
        List<String> telenorSzamlaSorszamok = szamlaDao.listTelenorSzamlaSorszamok();
        telenorSzamlaSorszamok.stream().filter(s -> s.equals(szamlak.get(0).getTelenorSzamlaSorszam())).findFirst()
                .ifPresent(s -> {throw new RuntimeException("Mar legeneralta a teteleket ehhez a telenor szamlahoz: " + s);});
        final AtomicLong latestIndex = new AtomicLong(szamlaDao.getLatestIndex());
        List<Long> id = new ArrayList<>();
        List<String> nev = new ArrayList<>();
        List<String> cim = new ArrayList<>();
        List<String> adoszam = new ArrayList<>();
        List<String> fizetesiMod = new ArrayList<>();
        List<Date> szamlaKelte = new ArrayList<>();
        List<Date> fizetesiHatarido = new ArrayList<>();
        List<Date> teljesites = new ArrayList<>();
        List<Date> idoszakKezdete = new ArrayList<>();
        List<Date> idoszakVege = new ArrayList<>();
        List<String> szamlaTetelek = new ArrayList<>();
        List<String> reszletezoTetelek = new ArrayList<>();
        List<String> ugyfelKod = new ArrayList<>();
        List<String> telenorSzamlaSorszam = new ArrayList<>();
        List<String> email = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
        szamlak.stream().filter(szamla1 -> szamla1.getSzamlaTetelek().length > 0).forEach(szamla -> {
            try {
                id.add(latestIndex.incrementAndGet());
                nev.add(szamla.getNev());
                cim.add(objectMapper.writeValueAsString(szamla.getCim()));
                adoszam.add(szamla.getAdoszam());
                fizetesiMod.add(szamla.getFizetesiMod());
                szamlaKelte.add(szamla.getKelte());
                fizetesiHatarido.add(szamla.getHatarido());
                teljesites.add(szamla.getTeljesites());
                idoszakKezdete.add(szamla.getIdoszakKezdete());
                idoszakVege.add(szamla.getIdoszakVege());
                szamlaTetelek.add(objectMapper.writeValueAsString(szamla.getSzamlaTetelek()));
                reszletezoTetelek.add(objectMapper.writeValueAsString(szamla.getTelefonszamok()));
                ugyfelKod.add(szamla.getUgyfelKod());
                telenorSzamlaSorszam.add(szamla.getTelenorSzamlaSorszam());
                email.add(szamla.getEmail());

            } catch (JsonProcessingException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });
        szamlaDao.bulkinsertSzamla(id, nev, cim, adoszam, fizetesiMod, szamlaKelte, fizetesiHatarido, teljesites,
                idoszakKezdete, idoszakVege, szamlaTetelek, reszletezoTetelek, ugyfelKod, telenorSzamlaSorszam, email);
        return szamlaDao.listByTelenorSzamlaSorszam(telenorSzamlaSorszam.get(0));
    }

    public void insertSzamla(Szamla szamla) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        StringWriter writer = new StringWriter();
        objectMapper.writeValue(writer, szamla.getTelefonszamok());
        szamlaDao.insertSzamla(szamla.getId(), szamla.getNev(),
                objectMapper.writeValueAsString(szamla.getCim()),
                szamla.getAdoszam(),
                szamla.getFizetesiMod(),
                szamla.getKelte(),
                szamla.getHatarido(),
                szamla.getTeljesites(),
                szamla.getIdoszakKezdete(),
                szamla.getIdoszakVege(),
                objectMapper.writeValueAsString(szamla.getSzamlaTetelek()),
                objectMapper.writeValueAsString(szamla.getTelefonszamok()),
                szamla.getUgyfelKod(),
                szamla.getEmail());
    }

    @POST
    @Path("/generate")
    public List<Szamla> generateSzamlak(List<Szamla> szamlak) throws InterruptedException, DocumentException, IOException {
        szamlazoProgramConnector.generateSzamlak(szamlak.stream()
                .filter(szamla1 -> szamla1.getSzamlaTetelek().length > 0)
                .collect(Collectors.toList()));
        return szamlaDao.listByTelenorSzamlaSorszam(szamlak.get(0).getTelenorSzamlaSorszam());
    }

    @GET
    @Path("/collect/szamla")
    @Produces("application/pdf")
    public StreamingOutput collectSzamlak(@QueryParam("ids") List<Long> ids){
        List<Szamla> szamlak = fetchSzamlakById(ids);
        return pdfRenderService.mergeSzamlak(szamlak);
    }

    @GET
    @Path("/collect/melleklet")
    @Produces("application/pdf")
    public StreamingOutput collectMellekletek(@QueryParam("ids") List<Long> ids){
        List<Szamla> szamlak = fetchSzamlakById(ids);
        return pdfRenderService.mergeMellekletek(szamlak);
    }

    @GET
    @Path("/collect/csekk")
    @Produces("application/pdf")
    public StreamingOutput collectCsekkek(@QueryParam("ids") List<Long> ids){
        List<Szamla> szamlak = fetchSzamlakById(ids);
        return pdfRenderService.mergeCsekkek(szamlak);
    }

    @GET
    @Path("/collect/szamla_melleklet")
    @Produces("application/pdf")
    public StreamingOutput collectSzamlaWithMelleklet(@QueryParam("ids") List<Long> ids){
        List<Szamla> szamlak = fetchSzamlakById(ids);
        return pdfRenderService.mergeSzamlakWithReszletezo(szamlak);
    }

    @GET
    @Path("/collect/email")
    public List<Map<String, String>> collectSzamlaForEmail(@QueryParam("ids") List<Long> ids){
        List<Map<String, String>> resp = new ArrayList<>();
        List<Szamla> szamlak = fetchSzamlakById(ids).stream()
            .filter(szamla1 -> szamla1.getSzamlaKepUrl() != null)
            .filter(szamla1 -> szamla1.getFizetesiMod().equals("ATUTALAS"))
            .collect(Collectors.toList());

        Session session = emailSenderService.createSession();
        for (Szamla szamla : szamlak) {
            try{
                resp.add(emailSenderService.send(szamla, session));
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return resp;
    }

    private List<Szamla> fetchSzamlakById(List<Long> ids){
        Handle h = jdbi.open();
        String inClause = ids.stream().map(Object::toString).collect(Collectors.joining(", "));
        Query<Map<String, Object>> results = h.createQuery("select * from szamla where szamla_id in (" + inClause + ") order by szamla_sorszam");
        List<Szamla> szamlak = results.map((index, r, ctx) -> new Szamla.SzamlaMapper().map(index, r, ctx)).list();
        h.close();
        return szamlak;
    }

    @GET
    @Path("/pdf/szamla/{from}/{to}/{type}")
    @Produces("application/pdf")
    public StreamingOutput mergeSzamlak(@PathParam("from") long from, @PathParam("to")long to, @PathParam("type") String type) throws ParseException {
        List<Szamla> szamlak = szamlaDao.findSzamlaByKelteBetween(new Date(from), new Date(to)).stream()
                .filter(szamla -> type.equals("ALL") || szamla.getFizetesiMod().equals(type)).collect(Collectors.toList());
        return pdfRenderService.mergeSzamlak(szamlak);
    }

    @GET
    @Path("/pdf/csekk/{from}/{to}/{type}")
    @Produces("application/pdf")
    public StreamingOutput generateCsekk(@PathParam("from") Long from, @PathParam("to")Long to, @PathParam("type") String type) throws ParseException {
        List<Szamla> szamlak = szamlaDao.findSzamlaByKelteBetween(new Date(from), new Date(to)).stream()
                .filter(szamla -> type.equals("ALL") || szamla.getFizetesiMod().equals(type)).collect(Collectors.toList());
        return pdfRenderService.mergeCsekkek(szamlak);
    }

    @GET
    @Path("/pdf/melleklet/{from}/{to}/{type}")
    @Produces("application/pdf")
    public StreamingOutput generateMellekletek(@PathParam("from") long from, @PathParam("to") long to, @PathParam("type") String type) throws ParseException {
        List<Szamla> szamlak = szamlaDao.findSzamlaByKelteBetween(new Date(from), new Date(to)).stream()
                .filter(szamla -> type.equals("ALL") || szamla.getFizetesiMod().equals(type)).collect(Collectors.toList());
        return pdfRenderService.mergeMellekletek(szamlak);
    }

    @GET
    @Path("/pdf/{ugyfelId}")
    @Produces("application/pdf")
    public StreamingOutput generatePdf(@PathParam("ugyfelId") Long ugyfelId) throws IOException {
        Szamla szamla = szamlaDao.findSzamlaById(ugyfelId);
        File szamlaKep = new File(elektra.getDataStore() + szamla.getSzamlaKepUrl().substring(4,szamla.getSzamlaKepUrl().length()));
        PdfReader reader = new PdfReader(szamlaKep.getAbsolutePath());
        return output -> {
            try {
                Document reszletezoDocument = new Document();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                PdfWriter.getInstance(reszletezoDocument, bos);
                reszletezoDocument.open();
                ReszletezoNyomtato.nyomtat(reszletezoDocument, szamla);
                reszletezoDocument.newPage();
                reszletezoDocument.close();

                PdfReader reszletezoReader = new PdfReader(bos.toByteArray());

                Document document = new Document();
                PdfCopy copy = new PdfCopy(document, output);
                document.open();

                for (int i = 1; i <= reader.getNumberOfPages(); i++) {
                    copy.addPage(copy.getImportedPage(reader, i));
                }

                for (int i = 1; i <= reszletezoReader.getNumberOfPages(); i++) {
                    copy.addPage(copy.getImportedPage(reszletezoReader, i));
                }
                document.close();
                reader.close();
                reszletezoReader.close();
            } catch (DocumentException e) {
                throw new WebApplicationException(e);
            }
        };
    }

    @GET
    @Path("/email/{from}/{to}")
    public void sendSzamlaInEmails(@PathParam("from") long from, @PathParam("to") long to) throws MessagingException {
        List<Szamla> szamlak = szamlaDao.findSzamlaByKelteBetween(new Date(from), new Date(to)).stream()
                .filter(szamla1 -> szamla1.getSzamlaKepUrl() != null)
                .filter(szamla1 -> szamla1.getFizetesiMod().equals("ATUTALAS"))
                .collect(Collectors.toList());
        Session session = emailSenderService.createSession();
        for (Szamla szamla : szamlak) {
            try{
                emailSenderService.send(szamla, session);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
