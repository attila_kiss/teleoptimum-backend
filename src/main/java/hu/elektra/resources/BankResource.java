package hu.elektra.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.elektra.core.Bank;
import hu.elektra.core.Szamla;
import hu.elektra.db.BankDao;
import hu.elektra.db.SzamlaDao;
import hu.elektra.core.UgyfelSzamlak;
import hu.elektra.service.BankReaderService;
import hu.elektra.service.BefizetesAssignerService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.skife.jdbi.v2.DBI;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Path("/bank")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@PermitAll
@AllArgsConstructor
@Log
public class BankResource {

    final BankDao bankDao;
    final BankReaderService bankReaderService;
    final BefizetesAssignerService befizetesAssignerService;
    final SzamlaDao szamlaDao;
    final DBI dbi;

    @GET
    public List<Bank> getAll(){
        return bankDao.list();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public List<Bank> uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {

        List<Bank> parsedBankData = bankReaderService.readExcel(uploadedInputStream);
        List<UgyfelSzamlak> szamlak = szamlaDao.listSzamlak();
        List<Bank> ret = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        befizetesAssignerService.assignBefizetesToUgyfel(parsedBankData, szamlak);
        parsedBankData.forEach(bank -> {
            if (bank.getSzamlaSorszamok() != null && bank.getSzamlaSorszamok().length <= 0){
                List<Szamla> ugyfelSzamlak = szamlaDao.findSzamlakByUgyfelKod(bank.getUgyfelKod());
                befizetesAssignerService.assignSzamlaBasedOnValue(bank, ugyfelSzamlak);
            }
        });
        parsedBankData.stream().filter(bank -> bank.getOsszeg() > 0).forEach(bank -> {
            try{
                bankDao.insertBank(bank.getDatum(), bank.getLeiras(), bank.getOsszeg(), bank.getUgyfelKod(), mapper.writeValueAsString(bank.getSzamlaSorszamok()));
                ret.add(bank);
            }catch (Exception e){
                log.warning("Nem sikerult a mentese a kovetkezo bank adatnak: " + bank.getLeiras());
                log.warning("Az ok: " + e.getMessage());
            }
        });
        return ret;
    }

    @PUT
    public void updateBank(List<Bank> befizetesek){
        ObjectMapper mapper = new ObjectMapper();
        befizetesek.forEach(bank -> {
            try {
                bankDao.updateBank(bank.getDatum(), bank.getLeiras(), bank.getOsszeg(), bank.getUgyfelKod(), mapper.writeValueAsString(bank.getSzamlaSorszamok()));
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        });
    }

}
