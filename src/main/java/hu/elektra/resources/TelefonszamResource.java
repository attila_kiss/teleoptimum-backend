package hu.elektra.resources;

import hu.elektra.core.Telefonszam;
import hu.elektra.db.TelefonszamDao;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/telefonszamok")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@PermitAll
public class TelefonszamResource {

    final TelefonszamDao telefonszamDao;

    public TelefonszamResource(TelefonszamDao telefonszamDao){
        this.telefonszamDao = telefonszamDao;
    }

    @GET
    public List<Telefonszam> getAll(){
        return telefonszamDao.list();
    }

    @POST
    public Response insertTelefonszam(List<Telefonszam> telefonszamok){
        telefonszamok.forEach(telefonszamDao::insert);
        return Response.status(Response.Status.OK).build();
    }

    @PUT
    public Response updateTelefonszam(List<Telefonszam> telefonszamok){
        telefonszamok.forEach(telefonszamDao::update);
        return Response.status(Response.Status.OK).build();
    }

    @DELETE
    public Response deleteTelefonszam(List<Telefonszam> telefonszamok){
        telefonszamok.forEach(telefonszam -> {
            telefonszamDao.delete(telefonszam.getId());
        });
        return Response.status(Response.Status.OK).build();
    }
}
