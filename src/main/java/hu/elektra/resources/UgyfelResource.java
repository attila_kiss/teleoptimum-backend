package hu.elektra.resources;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.elektra.core.Telefonszam;
import hu.elektra.core.Ugyfel;
import hu.elektra.db.TelefonszamDao;
import hu.elektra.db.UgyfelDao;
import io.dropwizard.auth.Auth;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.security.Principal;
import java.util.List;

@Path("/ugyfelek")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@PermitAll
public class UgyfelResource {

    private final UgyfelDao ugyfelDao;
    private final TelefonszamDao telefonszamDao;

    public UgyfelResource(UgyfelDao ugyfelDao, TelefonszamDao telefonszamDao){
        this.ugyfelDao = ugyfelDao;
        this.telefonszamDao = telefonszamDao;
    }

    @GET
    public List<Ugyfel> list(){
        List<Ugyfel> ugyfelek = ugyfelDao.list();
        List<Telefonszam> telefonszamok = telefonszamDao.list();
        mergeTelefonszamUgyfel(ugyfelek, telefonszamok);
        return ugyfelek;
    }

    private void mergeTelefonszamUgyfel(List<Ugyfel> ugyfelek, List<Telefonszam> telefonszamok){
        telefonszamok.stream().forEach(telefonszam -> {
            ugyfelek.stream().filter(ugyfel -> ugyfel.getId() == telefonszam.getUgyfelId()).findFirst()
                    .ifPresent(ugyfel1 -> ugyfel1.getTelefonszamok().add(telefonszam));
        });
    }

    @POST
    public int saveUgyfel(Ugyfel ugyfel) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
        if (ugyfel.getId() != null){
            return ugyfelDao.updateUgyfel(
                    ugyfel.getId(),
                    ugyfel.getUgyfelTipus(),
                    ugyfel.getEmail(),
                    ugyfel.getFizetesiMod(),
                    ugyfel.getNeedsSpecialInvoice(),
                    ugyfel.getNev(),
                    ugyfel.getSzamlaSzam(),
                    ugyfel.getUgyfelKod(),
                    ugyfel.getAdoszam(),
                    ugyfel.getCeg_jegyzek_szam(),
                    ugyfel.getKacsolat_tarto_nev(),
                    ugyfel.getKapcsolat_tarto_sz_ig_szam(),
                    ugyfel.getKapcsolat_tarto_tsz(),
                    ugyfel.getAnyja_neve(),
                    ugyfel.getEgyeb_telefonszam(),
                    ugyfel.getSz_hely_ido(),
                    ugyfel.getSzem_ig_szam(),
                    objectMapper.writeValueAsString(ugyfel.getCimek())
            );
        }else{
            return ugyfelDao.insertUgyfel(
                    ugyfelDao.getLatestUgyfelId() + 1L,
                    ugyfel.getUgyfelTipus(),
                    ugyfel.getEmail(),
                    ugyfel.getFizetesiMod(),
                    ugyfel.getNeedsSpecialInvoice(),
                    ugyfel.getNev(),
                    ugyfel.getSzamlaSzam(),
                    Integer.toString(ugyfelDao.getLatestUgyfelKod() + 1),
                    ugyfel.getAdoszam(),
                    ugyfel.getCeg_jegyzek_szam(),
                    ugyfel.getKacsolat_tarto_nev(),
                    ugyfel.getKapcsolat_tarto_sz_ig_szam(),
                    ugyfel.getKapcsolat_tarto_tsz(),
                    ugyfel.getAnyja_neve(),
                    ugyfel.getEgyeb_telefonszam(),
                    ugyfel.getSz_hely_ido(),
                    ugyfel.getSzem_ig_szam(),
                    objectMapper.writeValueAsString(ugyfel.getCimek())
            );
        }

    }
}
